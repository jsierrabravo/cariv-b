import { Clipboard } from 'react-bootstrap-icons';
//import 'bootstrap-icons/fonts'

let content;

export function ShowCommand({ id, command }) {
  function copy(e) {
    const btn = e.target;
    content = btn.previousElementSibling.innerHTML;
    navigator.clipboard.writeText(content).then(() => console.log('Copied'));
  }

  return (
    // <div class="d-flex justify-content-between align-items-center mb-3"> <span class="text-line me-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod</span> <button onclick="copy('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod','#copy_button_1')" id="copy_button_1" class="btn btn-sm btn-success copy-button">Copy</button> </div>

    <div
      className='d-flex justify-content-between align-items-center alert alert-success alert-dismissible fade show align-middle'
      role='alert'
    >
      <div className='col-6 text-left copy'>{command}</div>
      <button
        className='btn btn-outline-success btn-copy'
        id={`copy_button_${id}`}
        onClick={(e) => copy(e)}
      >
        <Clipboard />
      </button>
    </div>
  );
}
