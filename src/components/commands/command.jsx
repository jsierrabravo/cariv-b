import { ShowCommand } from "./showCommand";

export function Command({id, comando, descripcion}) {
    
    return (
        <div className="mx-auto" style={{width: "60%"}}>
            {/* <h3>{id}</h3> */}
            <p>{descripcion}</p>
            <ShowCommand id={id} command={comando}/>
            <hr />
        </div>
    );
}