import { Link } from 'react-router-dom';

export function Menu() {
  return (
    <nav
      id='navbar-example3'
      className='navbar navbar-dark bg-dark flex-column align-items-stretch p-3'
    >
      <Link className='navbar-brand' to='/'>
        Introducción a Git
      </Link>

      <Link className='navbar-brand' to='/instalacion-git'>
        Instalación de Git
      </Link>

      <Link className='navbar-brand' to='/quickstart'>
        Quickstart Git y Trabajando en paralelo
      </Link>

      <Link className='navbar-brand' to='/intro-gitlab'>
        Introducción a GitLab
      </Link>

      <Link className='navbar-brand' to='/profundizando'>
        Profundizando en Git
      </Link>
    </nav>
  );
}
