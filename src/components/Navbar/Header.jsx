import { Link } from "react-router-dom";
import { Search } from "../Search/Search";
import logo from "../../assets/images/logo-g2.png";
import { Menu } from "../Menu/Menu";

export function Header() {
  return (
    <div>
      <header className="fixed-top">
        <div className="px-3 py-2 bg-dark text-white">
          <div className="d-flex align-items-center justify-content-between">
            <div className="d-flex" style={{ gap: "50px" }}>
              <Search />
              <Link
                to="/"
                className="d-flex col-6 align-items-center my-2 my-lg-0 me-lg-auto text-white text-decoration-none"
              >
                <img src={logo} alt="Logo Grupo 2 CAR IV" id="logo" />
              </Link>
            </div>

            <ul className="nav col-6 col-lg-auto my-2 justify-content-center my-md-0 text-small">
              <li>
                <Link to="/" className="nav-link text-white">
                  Home
                </Link>
              </li>
              <li>
                <Link to="/About" className="nav-link text-white">
                  About
                </Link>
              </li>
            </ul>
          </div>
        </div>
        <Menu />
      </header>
    </div>
  );
}
