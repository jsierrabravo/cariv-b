import { useState, useEffect } from 'react';
import { HeaderAbout } from '../Navbar/HeaderAbout';
import { StudentItem } from './StudentItem.jsx';
import {getAboutInfo} from "../../services/getAboutInfo"


export function StudentList() {
  let [estudiante, setEstudiante] = useState(null);
  useEffect(() => {
    getAboutInfo().then(setEstudiante)
  }, []);
  return (
    <div className='card-block' 
    style={{ marginTop: '130px',
            width: '1800px' }}>
      {/* <HeaderAbout /> */}
      <div className='row justify-content-md-center'>
        <div className='col-7'>
          {estudiante ? (
            estudiante.map((elemento) => {
              return <StudentItem key={elemento.id} {...elemento} />;
            })
          ) : (
            <div>Cargando...</div>
          )}
        </div>
      </div>
    </div>
  );
}
