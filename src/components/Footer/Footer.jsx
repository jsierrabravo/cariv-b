export function Footer() {
  return (
    <div className="">
      {/* <!-- Footer --> */}
      <footer className="bg-dark text-center text-white">
        {/* <!-- Grid container --> */}
        <div className="container p-4">
          {/* <!-- Section: Social media --> */}
          <section className="mb-4">
            {/* <!-- Facebook --> */}
            <a
              className="btn btn-outline-light btn-floating m-1"
              href="https://www.facebook.com/lsvtechcolombia11/"
              target="_blank"
              role="button"
            >
              <i className="fab fa-facebook-f"></i>
            </a>

            {/* <!-- Twitter --> */}
            <a
              className="btn btn-outline-light btn-floating m-1"
              href="https://twitter.com/techlsv"
              target="_blank"
              role="button"
            >
              <i className="fab fa-twitter"></i>
            </a>

            {/* <!-- Instagram --> */}
            <a
              className="btn btn-outline-light btn-floating m-1"
              href="https://www.instagram.com/lsv_tech/"
              target="_blank"
              role="button"
            >
              <i className="fab fa-instagram"></i>
            </a>

            {/* <!-- Linkedin --> */}
            <a
              className="btn btn-outline-light btn-floating m-1"
              href="https://co.linkedin.com/company/lsv-tech"
              target="_blank"
              role="button"
            >
              <i className="fab fa-linkedin-in"></i>
            </a>

            {/* <!-- Youtube --> */}
            <a
              className="btn btn-outline-light btn-floating m-1"
              href="https://www.youtube.com/channel/UC5gbK6qQflBM0fEObgEyYvw"
              target="_blank"
              role="button"
            >
              <i className="fa-brands fa-youtube"></i>
            </a>
          </section>
          {/* <!-- Section: Social media --> */}

          {/* <!-- Section: Text --> */}
          <section>
            <p className="mb-0">Sigue las redes de LSV-TECH</p>
          </section>
          {/* <!-- Section: Text --> */}

          {/* <!-- Section: Links --> */}
        </div>
        {/* <!-- Grid container --> */}

        {/* <!-- Copyright --> */}
        <div
          className="text-center p-3 mt-0"
          style={{ backgroundColor: "rgba(0, 0, 0, 0.2)" }}
        >
          © 2022 Copyright: Equipo 2 CAR IV{" "}
          <a
            className="text-white"
            href="https://lsv-tech.com/"
            target="_blank"
          >
            lsv-tech.com
          </a>
        </div>
        {/* <!-- Copyright --> */}
      </footer>
      {/* <!-- Footer --> */}
    </div>
  );
}
